module prescaler(
    input wire clk_in,
    output wire clk_out
);

//-- Number of bits of the prescaler
parameter N = 22;

//-- Register for implementing the N bit counter
reg [N-1:0] count = 0;

//-- The most significant bit goes through the output
assign clk_out = (N == 0) ? clk_in : count[N-1];

//-- Counter: increases upon a rising edge
always @(posedge(clk_in)) begin
  count <= count + 1;
end

endmodule
