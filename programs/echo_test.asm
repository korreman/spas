li $v1, 1
loop:
    lock1:
        lbu $t0, 0x4004($0)
        bne $t0, $v1, lock1
    lbu $v0, 0x4000($0)
    sbu $zero, 0x4004($0)
    
    lock2:
        lbu $t0, 0x400C($0)
        bne $t0, $zero, lock2
    sb $v0, 0x4008($0)
    sb $v1, 0x400C($0)
    
    j loop