start:
addiu $t1, $zero, 1
move $t2, $zero
loop:
    addu $v0, $t1, $t2
    move $t2, $t1
    move $t1, $v0
    addiu $t3, $v0, -0x100
    bltz $t3, loop
