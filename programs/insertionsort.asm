    li $v0, -1
    lock1:
        lbu $t0, 0x4004($0)
        beq $t0, $zero, lock1
    lbu $s1, 0x4000($0)
    sb $zero, 0x4004($0)
    addiu $a1, $s1, 0x1FFF
    li $a0, 0x1FFF

    li $v0, 0x1    #debug
    move $v0, $s1
recv_loop:
    lock2:
        lbu $t0, 0x4004($0)
        beq $t0, $zero, lock2
    lbu $t0, 0x4000($0)
    sb $zero, 0x4004($0)
    addiu $a0, $a0, 1
    sb $t0, ($a0)
    bne $a0, $a1, recv_loop

    li $v0, 0x2    #debug
    li $a0, 0x2000

sort: #start address in a0, end address in a1
    li $v0, 0x5    #debug
    move $t4, $a0
loop1:
    addiu $t4, $t4, 1
    lw $t2, ($t4)
    move $t0, $t4
loop2:
    beq $t0, $a0, done2
    lw $t1, -1($t0)
    subu $t3, $t2, $t1
    bgtz $t3, done2
    sw $t1, ($t0)
    subiu $t0, $t0, 1
    b loop2
done2:
    sw $t2 ($t0)
    bne $t4, $a1, loop1
    li $v0, 0x6    #debug

    li $a0, 0x2000
    li $v0, 0x3    #debug
    li $s4, 1
send_loop:
    lock3:
        lbu $t0, 0x400C($0)
        bne $t0, $zero, lock3
    lbu $t0, ($a0)
    sb $t0, 0x4008($0)
    sb $s4, 0x400C($0)
    addiu $a0, $a0, 1
    bne $a0, $a1, send_loop

    li $v0, 0x4    #debug