module rom (
    input clk,
    input [31:0] next_pc,
    input advance_pc,
    output reg [31:0] instruction
);

    // How many instructions to read?
    parameter ROM_SIZE = 46;

    reg [31:0] rom [0:ROM_SIZE-1];
    initial $readmemh("rom.hex", rom);

    always @ (posedge clk) begin
        if (advance_pc)
            instruction <= (next_pc >> 2) < ROM_SIZE ? rom[(next_pc >> 2)] : 0;
    end
endmodule

