module registers (
    input clk,
    input rst,

    input [4:0] write_reg,
    input [31:0] write_value,
    input wire write_enable,

    input [4:0] read_reg1,
    input [4:0] read_reg2,
    output [31:0] read_value1,
    output [31:0] read_value2,
    output [31:0] test_val
);

    // The state consists of 32 registers each containing 32 bits
    reg [31:0] registers [31:0];

    // Always read out register values
    assign read_value1 = (rst || read_reg1 == 0)? 0 : registers[read_reg1 & 31];
    assign read_value2 = (rst || read_reg2 == 0)? 0 : registers[read_reg2 & 31];
    assign test_val = rst ? 0 : registers[2];

    integer i;
    always @ (posedge clk) begin
        if (rst)
            for(i = 0; i < 32; i = i + 1)
                registers[i] <= 0;
        else if (write_enable)
            registers[write_reg & 31] <= write_value;
    end

    // This is only used for debugging, as iverilog does not show the
    // state of multi-dimensional arrays
    wire [31:0] regs1;
    wire [31:0] regs2;
    wire [31:0] regs3;
    wire [31:0] regs4;
    assign regs1 = registers[1];
    assign regs2 = registers[2];
    assign regs3 = registers[3];
    assign regs4 = registers[4];

endmodule

