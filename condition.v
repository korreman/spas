module condition (
    input [31:0] num, // needs to be result of subtraction
    input [2:0] cond, // conditions defined in constants.vh
    output cond_met
);
    // Branch condition satisfaction checker
    // Uses ALU result to find
    wire use_num;
    wire use_sign;
    wire negate;

    assign negate = cond[0];
    assign use_sign = cond[1];
    assign use_num = cond[2];

    assign cond_met = (((!num) && use_num) | (num[31] && use_sign)) ^ negate;

endmodule
