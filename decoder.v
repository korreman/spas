`include "constants.vh"

module decoder (
    input [5:0] opcode,
    input [5:0] funct,
    input [4:0] rt,
    input wire mem_ready,

    output wire mem_signed,
    output [2:0] mem_val_size,
    output wire mem_valid,
    output wire mem_write,
    output wire load,
    output wire reg_write,
    output wire write_to_rt,
    output wire write_to_ra,
    output wire use_imm,
    output wire jump_imm,
    output wire jump_reg,
    output wire link,
    output wire branch,
    output wire advance_pc,
    output wire [2:0] branch_cond,
    output wire [5:0] alu_op
);
    // Create a single register with bits for each signal. Assign values based
    // on inputs, and split into individual wires. Has the benefit over a ROM in
    // that we can match on wildcards.

    // Could be used to force down writing signals for stalling, without
    // changing any combinatorial values.
    `define SIG_CNT 26
    wire [`SIG_CNT-1:0] stall_mask = `SIG_CNT'b11111_11011_10010_11111_11111;

    reg [`SIG_CNT-1:0] signals;
    wire [`SIG_CNT-1:0] signals_masked;
    assign signals_masked = signals & ({`SIG_CNT{~ stall}} | stall_mask);
    assign advance_pc = ~ stall;

    wire use_funct;
    wire stall;
    wire [5:0] alu_imm;
    assign mem_signed   = signals_masked[25];
    assign mem_val_size = signals_masked[24:22];
    assign stall        = signals[21] & (~ mem_ready);
    assign mem_valid    = signals_masked[20] & stall;
    assign mem_write    = signals_masked[19];
    assign load         = signals_masked[18];
    assign reg_write    = signals_masked[17];
    assign write_to_rt  = signals_masked[16];
    assign write_to_ra  = signals_masked[15];
    assign use_imm      = signals_masked[14];
    assign jump_imm     = signals_masked[13];
    assign jump_reg     = signals_masked[12];
    assign link         = signals_masked[11];
    assign branch       = signals_masked[10];
    assign branch_cond  = signals_masked[9:7];
    assign use_funct    = signals_masked[6];
    assign alu_imm      = signals_masked[5:0];
    assign alu_op = use_funct ? funct : alu_imm;

    wire T = 1'b1;
    wire F = 1'b0;

    always @(*)
    begin
        casez ({opcode, rt, funct})
            //                                        sn   msize st mv mw ld rw w2t w2a ui ji jr ln  b   b_cond uf   alu_imm
            // Jump instructions
            {`OPCODE_J,      5'hzz, 6'hzz}: signals = {F, 3'b000, F, F, F, F, F,  F,  F, T, T, F, F, F, `C_NONE, F, `ALU_OP_PASS};
            {`OPCODE_JAL,    5'hzz, 6'hzz}: signals = {F, 3'b000, F, F, F, F, T,  F,  T, T, T, F, T, F, `C_NONE, F, `ALU_OP_PASS};
/*JR    */  {`OPCODE_ALU,    5'hzz, 6'h08}: signals = {F, 3'b000, F, F, F, F, F,  F,  F, F, F, T, F, F, `C_NONE, F, `ALU_OP_PASS};
/*JALR  */  {`OPCODE_ALU,    5'hzz, 6'h09}: signals = {F, 3'b000, F, F, F, F, T,  F,  F, F, F, T, T, F, `C_NONE, F, `ALU_OP_PASS};

/*BLTZ  */  {`OPCODE_BRANCH, 5'h00, 6'hzz}: signals = {F, 3'b000, F, F, F, F, F,  F,  F, F, F, F, F, T, `C_BLTZ, F, `ALU_OP_SUB };
/*BGEZ  */  {`OPCODE_BRANCH, 5'h01, 6'hzz}: signals = {F, 3'b000, F, F, F, F, F,  F,  F, F, F, F, F, T, `C_BGEZ, F, `ALU_OP_SUB };
/*BLTZAL*/  {`OPCODE_BRANCH, 5'h10, 6'hzz}: signals = {F, 3'b000, F, F, F, F, T,  F,  T, F, F, F, T, T, `C_BLTZ, F, `ALU_OP_SUB };
/*BGEZAL*/  {`OPCODE_BRANCH, 5'h11, 6'hzz}: signals = {F, 3'b000, F, F, F, F, T,  F,  T, F, F, F, T, T, `C_BGEZ, F, `ALU_OP_SUB };
            {`OPCODE_BEQ,    5'hzz, 6'hzz}: signals = {F, 3'b000, F, F, F, F, F,  F,  F, F, F, F, F, T, `C_BEQ,  F, `ALU_OP_SUB };
            {`OPCODE_BNE,    5'hzz, 6'hzz}: signals = {F, 3'b000, F, F, F, F, F,  F,  F, F, F, F, F, T, `C_BNE,  F, `ALU_OP_SUB };
            {`OPCODE_BLEZ,   5'hzz, 6'hzz}: signals = {F, 3'b000, F, F, F, F, F,  F,  F, F, F, F, F, T, `C_BLEZ, F, `ALU_OP_SUB };
            {`OPCODE_BGTZ,   5'hzz, 6'hzz}: signals = {F, 3'b000, F, F, F, F, F,  F,  F, F, F, F, F, T, `C_BGTZ, F, `ALU_OP_SUB };

            // 3-register ALU
            {`OPCODE_ALU,    5'hzz, 6'hzz}: signals = {F, 3'b000, F, F, F, F, T,  F,  F, F, F, F, F, F, `C_NONE, T, `ALU_OP_PASS};

            // Immediate ALU
            {`OPCODE_ADDIU,  5'hzz, 6'hzz}: signals = {F, 3'b000, F, F, F, F, T,  T,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_ADD };
            {`OPCODE_ADDI,   5'hzz, 6'hzz}: signals = {F, 3'b000, F, F, F, F, T,  T,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_ADD };
            {`OPCODE_SLTI,   5'hzz, 6'hzz}: signals = {F, 3'b000, F, F, F, F, T,  T,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_SLT };
            {`OPCODE_SLTIU,  5'hzz, 6'hzz}: signals = {F, 3'b000, F, F, F, F, T,  T,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_SLTU};
            {`OPCODE_ANDI,   5'hzz, 6'hzz}: signals = {F, 3'b000, F, F, F, F, T,  T,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_AND };
            {`OPCODE_ORI,    5'hzz, 6'hzz}: signals = {F, 3'b000, F, F, F, F, T,  T,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_OR  };
            {`OPCODE_XORI,   5'hzz, 6'hzz}: signals = {F, 3'b000, F, F, F, F, T,  T,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_XOR };

            // Memory
            {`OPCODE_LB ,    5'hzz, 6'hzz}: signals = {T, 3'b001, T, T, F, T, T,  T,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_ADD};
            {`OPCODE_LH ,    5'hzz, 6'hzz}: signals = {T, 3'b010, T, T, F, T, T,  T,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_ADD};
            {`OPCODE_LW,     5'hzz, 6'hzz}: signals = {F, 3'b100, T, T, F, T, T,  T,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_ADD};
            {`OPCODE_LBU,    5'hzz, 6'hzz}: signals = {F, 3'b001, T, T, F, T, T,  T,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_ADD};
            {`OPCODE_LHU,    5'hzz, 6'hzz}: signals = {F, 3'b010, T, T, F, T, T,  T,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_ADD};

            {`OPCODE_SB ,    5'hzz, 6'hzz}: signals = {F, 3'b001, T, T, T, F, F,  F,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_ADD};
            {`OPCODE_SH ,    5'hzz, 6'hzz}: signals = {F, 3'b010, T, T, T, F, F,  F,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_ADD};
            {`OPCODE_SW,     5'hzz, 6'hzz}: signals = {F, 3'b100, T, T, T, F, F,  F,  F, T, F, F, F, F, `C_NONE, F, `ALU_OP_ADD};
            default:                        signals = {F, 3'b000, F, F, F, F, F,  F,  F, F, F, F, F, F, `C_NONE, F, `ALU_OP_PASS};
        endcase
    end
endmodule
