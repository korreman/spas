module pc_control (
    input [31:0] next_pc,
    input [15:0] imm16_offs,
    input [25:0] imm26_addr,
    input [31:0] reg_addr,

    input wire cond_met,
    input wire is_branch,
    input wire is_jump_imm,
    input wire is_jump_reg,
    input wire rst,

    output [31:0] new_pc
);

wire [31:0] offset_fixed = $signed({imm16_offs, 2'b00});
wire [31:0] offset_pc;
assign offset_pc = next_pc + offset_fixed;

wire [31:0] imm_addr;
assign imm_addr = {next_pc[31:28], imm26_addr, 2'b00};

assign new_pc = rst ? 0 :
                (is_branch && cond_met) ? offset_pc :
                is_jump_imm ? imm_addr :
                is_jump_reg ? reg_addr :
                next_pc;

endmodule
