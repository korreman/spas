// performs translation of memory data values
module mem_convert(
    input [1:0] lower_bits,
    input [2:0] size,
    input sign_ext,
    input [31:0] write_data,
    input [31:0] read_data,
    input mem_write,
    output [3:0] wen_bits,
    output [31:0] write_result,
    output [31:0] read_result
);

    wire [3:0] temp;
    assign temp[0] = (size >= 1);
    assign temp[1] = (size >= 2);
    assign temp[2] = (size == 4);
    assign temp[3] = (size == 4);

    assign wen_bits = mem_write ? temp << lower_bits : 0;
    assign write_result = write_data << (8 * lower_bits);

    wire [31:0] read_shifted = read_data >> (8 * lower_bits);
    assign read_result = (read_shifted & read_mask) | sign_bits;

    wire [31:0] read_mask;
    assign read_mask = {{8{temp[3]}},{8{temp[2]}},{8{temp[1]}},{8{temp[0]}}};

    wire [31:0] sign_bits;
    assign sign_bits = {32{sign_ext}}
                     & (~read_mask)
                     & {32{read_shifted[size * 8 - 1]}};

endmodule
