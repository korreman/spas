module mem_uart (
    input clk,
    input rst,
    input [1:0] addr,
    input valid,
    input [31:0] wdata,
    input [3:0] wen,
    output reg [31:0] rdata,
    output reg ready,
    input rx,
    output tx
);
    reg [31:0] mem [0:3];
    reg [7:0] dataOut;
    reg send;

    wire [31:0] clkdiv = 104;
    wire busyIn;
    wire busyOut;
    wire recv;
    wire err;
    wire [7:0] dataIn;

    uart_tx uart_tx(
        .clk(clk),
        .rst(rst),
        .clkdiv(clkdiv),
        .send(send),
        .busy(busyOut),
        .data(dataOut),
        .tx(tx)
    );

    uart_rx uart_rx(
        .clk(clk),
        .rst(rst),
        .clkdiv(clkdiv),
        .recv(recv),
        .busy(busyIn),
        .err(err),
        .data(dataIn),
        .rx(rx)
    );

    always @ (posedge clk) begin
        send <= 0;
        ready <= 0;

        if (recv) begin
            mem[0] <= dataIn;
            mem[1] <= 1;
        end

        if (mem[3] && !busyOut) begin
            send <= 1;
            dataOut <= mem[2];
            mem[3] <= 0;
        end

        if (valid)
        begin
            ready <= 1;
            rdata <= mem[addr];
            if (wen[0]) mem[addr][7:0] <= wdata[7:0];
            if (wen[1]) mem[addr][15:8] <= wdata[15:8];
            if (wen[2]) mem[addr][23:16] <= wdata[23:16];
            if (wen[3]) mem[addr][31:24] <= wdata[31:24];
        end

        if (rst)
        begin
            mem[0] <= 0;
            mem[1] <= 0;
            mem[2] <= 0;
            mem[3] <= 0;
            dataOut <= 0;
            send <= 0;
            rdata <= 0;
            ready <= 0;
        end
    end

endmodule
