`include "constants.vh"

//TODO: Generate traps on unsigned instructions?

// Gets two inputs and an operation to compute on them
// and outputs the result of the operation.
module alu(
    input [31:0] a,
    input [31:0] b,
    input [5:0] aluOp,
    input [4:0] shamt,
    output reg [31:0] result
);
    // Even though this uses a reg, this is completely combinatorial.
    // The module does not take a clock, and does not compile into any
    // flip-flops. The reason for "reg" is because verilog requires it
    // before it can be used in a always @(*) block.

    always @(*)
    begin
        case (aluOp)
            `ALU_OP_SLL:  result = b << shamt;
            `ALU_OP_SRL:  result = b >> shamt;
            `ALU_OP_SRA:  result = b >>> shamt;

            `ALU_OP_ADD:  result = a + b;
            `ALU_OP_ADDU: result = a + b;
            `ALU_OP_SUB:  result = a - b;
            `ALU_OP_SUBU: result = a - b;

            `ALU_OP_AND:  result = a & b;
            `ALU_OP_OR:   result = a | b;
            `ALU_OP_XOR:  result = a ^ b;
            `ALU_OP_NOR:  result = ~(a | b);

            `ALU_OP_SLT:  result = ($signed(a) < $signed(b)) ? 1 : 0;
            `ALU_OP_SLTU: result = (a < b) ? 1 : 0;

            default: result = a;
        endcase
    end
endmodule
