# test various normal jumps
    move $v0, $zero
    j label1
dest:
    addiu $v0, $zero, 42
    j end
label1:
    j label4
    addiu $v0, $zero, -42
label2:
    j label3
    addiu $v0, $zero, -42
label3:
    j dest
    addiu $v0, $zero, -42
label4:
    j label2
    addiu $v0, $v0, -42
end:
# 42
