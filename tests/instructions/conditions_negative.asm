# testing positives on jumps - additions into result register aren't skipped
# init
    addiu $t0, $zero, 0
    addiu $t1, $zero, 5
    addiu $t2, $zero, 5
    addiu $t3, $zero, -7

# test
    beq $t1, $t3, skip1
    addiu $v0, $v0, 1
skip1:

    bne $t1, $t2, skip2
    addiu $v0, $v0, 1
skip2:

    bltz $t0, skip3
    addiu $v0, $v0, 1
skip3:

    bltz $t1, skip4
    addiu $v0, $v0, 1
skip4:

    bgtz $t0, skip5
    addiu $v0, $v0, 1
skip5:

    bgtz $t3, skip6
    addiu $v0, $v0, 1
skip6:

    blez $t1, skip7
    addiu $v0, $v0, 1
skip7:

    bgez $t3, skip8
    addiu $v0, $v0, 1
skip8:
# 8
