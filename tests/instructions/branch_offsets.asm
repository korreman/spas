# test both positive and negative branch offsets
    move $v0, $zero
    bgez $zero, label1
dest:
    addiu $v0, $zero, 42
    blez $zero, end
label1:
    bgez $zero, label4
label2:
    bgez $zero, label3
label3:
    bgez $zero, dest
label4:
    bgez $zero, label2
end:
# 42
