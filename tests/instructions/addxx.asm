# Test ADD, ADDU, ADDI, ADDIU
# Things to test:
# - I-instructions correctly source immediates.
# - I-instructions write to rt.
# - R-instructions correctly source registers.
# - R-instructions write to rd.
# - All instructions add together numbers.
# - Sourcing from $zero always gives us a 0 (writing to $zero has no effect).
# - All instructions perform the add operation.

addiu $t0, $zero, 13
addi  $t1, $zero, 14
addu  $t2, $t1, $t0
addi  $t3, $t2, 15
add   $zero, $t2, $t1
add   $t4, $t3, $zero
move  $v0, $t4
# 42
