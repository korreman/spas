# Test that memory operations function properly.
# LB  at offset 00, 01, 10, 11
# LBU at offset 00, 01, 10, 11
# LH  at offset 00, 01, 10
# LHU at offset 00, 01, 10
# LW  at offset 00
# SB  at offset 00, 01, 10, 11
# SH  at offset 00, 01, 10
# SW  at offset 00

li $4, 0x34
li $5, 0x7B
li $6, 0x5A
li $7, 0xFC

sb $4, 0x1004($0)
sb $5, 0x1005($0)
sb $6, 0x1006($0)
sb $7, 0x1007($0)

lb $8, 0x1004($0)
add $v0, $v0, $8


lb $8, 0x1006($0)
add $v0, $v0, $8

lb $8, 0x1007($0)
add $v0, $v0, $8

lh $8, 0x1004($0)
add $v0, $v0, $8

lh $8, 0x1006($0)
add $v0, $v0, $8

lbu $8, 0x1007($0)
add $v0, $v0, $8

lhu $8, 0x1006($0)
add $v0, $v0, $8

sh $8,  0x1008($0)
lhu $8, 0x1008($0)
add $v0, $v0, $8

sh $8,  0x100A($0)
lhu $8, 0x100A($0)
add $v0, $v0, $8

lw $8, 0x1004($0)
add $v0, $v0, $8

sw $8, 0x1010($0)
lw $8, 0x1010($0)
add $v0, $v0, $8

srl $9, $8, 8
add $v0, $v0, $9
srl $9, $8, 8
add $v0, $v0, $9
srl $9, $8, 8
add $v0, $v0, $9

# Expected: 0xfbad73fb, calculated with MARS