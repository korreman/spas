# uses linking to call routines and return
jal routine1
jal routine2
bgezal $zero, routine3
addiu $t1, $zero, -1
bltzal $t1, routine4
jalr $ra, $t0
j end

routine3:
    addiu $v0, $v0, 13
    jr $ra
    addiu $v0, $v0, 100
routine2:
    addiu $v0, $v0, 6
    jalr $t0, $ra
    addiu $v0, $v0, 129
routine1:
    addiu $v0, $zero, 8
    jr $ra
    addiu $v0, $v0, 59
routine4:
    addiu $v0, $v0, 5
    jalr $t0, $ra
# continues here, sort of like a coroutine
   addiu $v0, $v0, 10
   jr $ra

end:
# 42
