//-- Test bench module
module instruction_tb;

reg clk = 0;
wire [31:0] test_val;

// Do not prescale on the test bench
parameter PRESCALE_BITS = 0;

//-- Place the component (it's actually called "instance") and
//-- connect the A cable to the A pin.
cpu #(.PRESCALE_BITS(PRESCALE_BITS)) T(
  .clk_in(clk),
  .test_val(test_val)
);
always #1 clk = ~clk;

//-- Begin the test (Checking block)
initial begin

  //-- Define a file to dump all the data.
  $dumpfile("cpu.vcd");

  //-- Dump all the data into that file when simulation finishes
  $dumpvars(0, T);

  # 10000 $finish;
end
endmodule
