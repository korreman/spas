module uart_tx (
    clk, rst,
    clkdiv,
    send, busy,
    data,
    tx,
);
    input clk; input rst;
    input [31:0] clkdiv;
    input send; output reg busy;
    input [7:0] data;
    output tx;

    reg [9:0] pattern;
    reg [3:0] bitcnt;

    reg [31:0] clkcnt;

    assign tx = pattern[0];

    always @ (posedge clk) if(rst) begin
        busy <= 0;
        clkcnt <= 0;
        bitcnt <= 0;
        pattern <= 10'b1111111111;
    end else if(!busy) begin
        pattern <= 10'b1111111111;
        busy <= 0;
        if(send) begin
            busy <= 1;
            pattern <= {1'b1, data, 1'b0};
            bitcnt <= 11;
            clkcnt <= clkdiv;
        end
    end else begin
        clkcnt <= clkcnt - 1;
        if(clkcnt == 0) begin
            clkcnt <= clkdiv;
            pattern <= {1'b1, pattern[9:1]};
            bitcnt <= bitcnt - 1;
            if(bitcnt == 0) busy <= 0;
        end
    end
endmodule
