module mem_controller (
    input clk,
    input rst,
    input [29:0] addr,
    input valid,
    input [31:0] wdata,
    input [3:0] wen,
    output wire [31:0] rdata,
    output wire ready,
    input rx,
    output tx
);

    wire inUartRange = (29'h1000 <= addr) && (addr < 29'h1004);
    wire inRamRange = (29'h0 <= addr) && (addr < 29'h1000);

    wire [1:0] uartAddr = addr[1:0];
    wire uartValid = inUartRange && valid;
    wire [3:0] uartWen = uartValid ? wen : 4'b0000;
    wire [31:0] uartRdata;
    wire uartReady;
    mem_uart uart(
        .clk(clk),
        .rst(rst),
        .addr(uartAddr),
        .valid(uartValid),
        .wdata(wdata),
        .wen(uartWen),
        .rdata(uartRdata),
        .ready(uartReady),
        .rx(rx),
        .tx(tx)
    );

    wire [11:0] ramAddr = addr[11:0];
    wire ramValid = inRamRange && valid;
    wire [3:0] ramWen = ramValid ? wen : 4'b0000;
    wire [31:0] ramRdata;
    wire ramReady;
    mem_ram ram(
        .clk(clk),
        .addr(ramAddr),
        .valid(ramValid),
        .wdata(wdata),
        .wen(ramWen),
        .rdata(ramRdata),
        .ready(ramReady)
    );

    wire nullValid = !(inRamRange || inUartRange) && valid;
    wire [31:0] nullRdata;
    wire nullReady;
    mem_null null(
        .clk(clk),
        .valid(nullValid),
        .rdata(nullRdata),
        .ready(nullReady)
    );

    assign ready = nullReady || ramReady || uartReady;
    assign rdata = ramReady ? ramRdata :
                   uartReady ? uartRdata :
                   nullRdata;

endmodule