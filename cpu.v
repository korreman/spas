module cpu (
    input clk_in,
    output wire [31:0] test_val,
    input rx,
    output tx
);
    // Prescaler, to run the CPU slower on the device.
    // This is to make sure we can see what's going on by looking
    // at the test_val.
    parameter PRESCALE_BITS = 22;
    // wire clk;
    // prescaler #(.N(PRESCALE_BITS))
    //     myPrescaler(
    //         .clk_in(clk_in),
    //         .clk_out(clk)
    //     );
    wire clk;
    assign clk = clk_in;

    // Have a reset pin and set it low after the first clock cycle
    // This is used to make sure that we skip doing anything the first
    // clock cycle, since the rom has not yet fetched anything
    reg [11:0] rst_counter = 0;
    reg rst = 1;
    always @ (posedge clk) begin
        rst_counter <= rst_counter + 1;
        if (rst_counter[11])
            rst <= 0;
    end

    // Find the next chronological PC.
    reg [31:0] pc;
    wire [31:0] next_pc;
    assign next_pc = (rst) ? 0 : pc + 4;

    wire branch;
    wire [2:0] branch_cond;
    wire cond_met;

    wire [31:0] result;
    condition my_condition (
        .num(result),
        .cond(branch_cond),
        .cond_met(cond_met)
    );

    wire [31:0] new_pc;
    wire [31:0] read_value1;

    wire jump_imm;
    wire jump_reg;

    pc_control my_pc_control(
      .next_pc(next_pc),
      .imm16_offs(instruction[15:0]),
      .imm26_addr(instruction[25:0]),
      .reg_addr(read_value1),
      .cond_met(cond_met),
      .is_jump_imm(jump_imm),
      .is_jump_reg(jump_reg),
      .is_branch(branch),
      .rst(rst),
      .new_pc(new_pc)
    );

    wire advance_pc;
    always @ (posedge clk) begin
        if (advance_pc)
            pc <= new_pc;
    end

    // We read from the rom to get the instruction
    // We read from the next_pc, so it is read to
    // execute in the next clock cycle.
    wire [31:0] instruction;
    rom myRom(
        .clk(clk),
        .advance_pc(advance_pc),
        .next_pc(new_pc),
        .instruction(instruction)
    );

    // Decode the instruction. Some of this is just wire manipulation,
    // while the actual decoding happens inside the decoder.
    wire [5:0] opcode;
    wire [4:0] rs;
    wire [4:0] rt;
    wire [4:0] rd;
    wire [4:0] shamt;
    wire [5:0] funct;
    wire [31:0] imm;
    assign opcode = instruction[31:26];
    assign rs = instruction[25:21];
    assign rt = instruction[20:16];
    assign rd = instruction[15:11];
    assign shamt = instruction[10:6];
    assign funct = instruction[5:0];
    assign imm = $signed(instruction[15:0]);

    wire mem_signed;
    wire [2:0] mem_val_size;
    wire mem_ready;
    wire mem_valid;
    wire mem_write;
    wire mem_load;
    wire reg_write;
    wire write_to_rt;
    wire write_to_ra;
    wire use_imm;
    wire link;
    wire [5:0] alu_op;

    decoder myDecoder(
        .opcode(opcode),
        .funct(funct),
        .rt(rt),
        .mem_ready(mem_ready),

        .mem_signed(mem_signed),
        .mem_val_size(mem_val_size),
        .mem_valid(mem_valid),
        .mem_write(mem_write),
        .load(mem_load),
        .reg_write(reg_write),
        .write_to_rt(write_to_rt),
        .write_to_ra(write_to_ra),
        .use_imm(use_imm),
        .jump_imm(jump_imm),
        .jump_reg(jump_reg),
        .link(link),
        .branch(branch),
        .advance_pc(advance_pc),
        .branch_cond(branch_cond),
        .alu_op(alu_op)
    );

    // Set up the register file
    wire [4:0] read_reg1;
    wire [4:0] read_reg2;
    wire [4:0] write_reg;
    assign read_reg1 = rs;
    assign read_reg2 = rt;
    assign write_reg = write_to_rt ? rt :
                       write_to_ra ? 31 :
                       rd;

    wire [31:0] read_value2;
    wire [31:0] write_value;

    registers myRegisters(
        .clk(clk),
        .rst(rst),
        .read_reg1(read_reg1),
        .read_reg2(read_reg2),
        .write_reg(write_reg),
        .read_value1(read_value1),
        .read_value2(read_value2),
        .write_value(write_value),
        .test_val(test_val),
        .write_enable(reg_write)
    );

    wire [31:0] alu_in_b;
    assign alu_in_b = use_imm ? imm : read_value2;

    // Actually do the computation and feed the result back into the register file
    alu myAlu(
        .a(read_value1),
        .b(alu_in_b),
        .aluOp(alu_op),
        .shamt(shamt),
        .result(result)
    );

    assign write_value = link ? next_pc:
                         mem_load ? mem_read_trns : result;

    wire [3:0] mem_write_enable;
    wire [31:0] mem_read_data;
    wire [31:0] mem_read_trns;
    wire [31:0] mem_write_trns;

    mem_convert myMemoryConverter (
        .lower_bits(result[1:0]),
        .size(mem_val_size),
        .sign_ext(mem_signed),
        .write_data(read_value2),
        .read_data(mem_read_data),
        .wen_bits(mem_write_enable),
        .mem_write(mem_write),
        .write_result(mem_write_trns),
        .read_result(mem_read_trns)
    );

    mem_controller myMemory (
        .clk(clk),
        .rst(rst),
        .addr(result[31:2]),
        .valid(mem_valid),
        .wdata(mem_write_trns),
        .wen(mem_write_enable),

        .rdata(mem_read_data),
        .ready(mem_ready),
        .rx(rx),
        .tx(tx)
    );

endmodule
