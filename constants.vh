`define ALU_OP_SLL  6'h00
`define ALU_OP_SRL  6'h02
`define ALU_OP_SRA  6'h03

`define ALU_OP_ADD  6'h20
`define ALU_OP_ADDU 6'h21
`define ALU_OP_SUB  6'h22
`define ALU_OP_SUBU 6'h23

`define ALU_OP_AND  6'h24
`define ALU_OP_OR   6'h25
`define ALU_OP_XOR  6'h26
`define ALU_OP_NOR  6'h27

`define ALU_OP_SLT  6'h2A
`define ALU_OP_SLTU 6'h2B

`define ALU_OP_PASS 6'h3F

`define OPCODE_ALU    6'b000000
`define OPCODE_BRANCH 6'b000001
`define OPCODE_J      6'b000010
`define OPCODE_JAL    6'b000011
`define OPCODE_BEQ    6'b000100
`define OPCODE_BNE    6'b000101
`define OPCODE_BLEZ   6'b000110
`define OPCODE_BGTZ   6'b000111
`define OPCODE_ADDI   6'b001000
`define OPCODE_ADDIU  6'b001001
`define OPCODE_SLTI   6'b001010
`define OPCODE_SLTIU  6'b001011
`define OPCODE_ANDI   6'b001100
`define OPCODE_ORI    6'b001101
`define OPCODE_XORI   6'b001110
`define OPCODE_LB     6'b100000
`define OPCODE_LH     6'b100001
`define OPCODE_LW     6'b100011
`define OPCODE_LBU    6'b100100
`define OPCODE_LHU    6'b100101
`define OPCODE_SB     6'b101000
`define OPCODE_SH     6'b101001
`define OPCODE_SW     6'b101011

`define C_NONE  3'b000
`define C_BLTZ  3'b010
`define C_BGEZ  3'b011
`define C_BEQ   3'b100
`define C_BNE   3'b101
`define C_BLEZ  3'b110
`define C_BGTZ  3'b111
