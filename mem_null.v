module mem_null (
    input clk,
    input valid,
    output [31:0] rdata,
    output reg ready
);

    assign rdata = 0;

    always @ (posedge clk) begin
        ready <= valid;
    end
endmodule