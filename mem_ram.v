module mem_ram (
    input clk,
    input [ADDR_BITS-1:0] addr,
    input valid,
    input [31:0] wdata,
    input [3:0] wen,
    output reg [31:0] rdata,
    output reg ready
);
    parameter ADDR_BITS = 12;

    reg [31:0] mem [0:(1 << ADDR_BITS)-1];

    always @ (posedge clk) begin
        if (!valid)
        begin
            ready <= 0;
        end
        else
        begin
            ready <= 1;
            rdata <= mem[addr];
            if (wen[0]) mem[addr][7:0] <= wdata[7:0];
            if (wen[1]) mem[addr][15:8] <= wdata[15:8];
            if (wen[2]) mem[addr][23:16] <= wdata[23:16];
            if (wen[3]) mem[addr][31:24] <= wdata[31:24];
        end
    end

    // This is only used for debugging, as iverilog does not show the
    // state of multi-dimensional arrays
    wire [31:0] mem0 = mem[0];
    wire [31:0] mem1 = mem[1];
    wire [31:0] mem2 = mem[2];
    wire [31:0] mem3 = mem[3];
endmodule