TARGET_STEM = cpu

PINS_FILE = pins.pcf

YOSYS_LOG  = synth.log
YOSYS_ARGS = -v1 -l $(YOSYS_LOG)

VERILOG_SRCS := cpu.v $(shell find $(SOURCEDIR) -name '*.v' -not -name "cpu.v" -not -name '*_tb.v')
# VERILOG_SRCS = cpu.v
VERILOG_TB_SRCS := $(shell find $(SOURCEDIR) -name '*_tb.v')

BIN_FILE  = $(TARGET_STEM).bin
ASC_FILE  = $(TARGET_STEM).asc
BLIF_FILE = $(TARGET_STEM).blif
VPP_FILE = $(TARGET_STEM).vpp
VCD_FILE = $(TARGET_STEM).vcd

all:    $(BIN_FILE)

$(BLIF_FILE):    $(VERILOG_SRCS) rom.hex
	yosys $(YOSYS_ARGS) -p "synth_ice40 -blif $(BLIF_FILE)" $(VERILOG_SRCS)

$(ASC_FILE):    $(BLIF_FILE) $(PINS_FILE)
	arachne-pnr -d $(ARACHNE_DEVICE) -P $(PACKAGE) -o $(ASC_FILE) -p $(PINS_FILE) $<

$(BIN_FILE):    $(ASC_FILE)
	icepack    $< $@

prog:    $(BIN_FILE)
	$(PROG_BIN) $<

timings:$(ASC_FILE)
	icetime -tmd $(ICETIME_DEVICE) $<

sim: $(VERILOG_SRCS) $(VERILOG_TB_SRCS) rom.hex
	iverilog -o $(VPP_FILE) $(VERILOG_SRCS) $(VERILOG_TB_SRCS)
	./$(VPP_FILE)
	gtkwave ./$(VCD_FILE)

clean:
	rm -f $(BIN_FILE) $(ASC_FILE) $(BLIF_FILE) $(YOSYS_LOG) $(VPP_FILE) $(VCD_FILE)

.PHONY:    all clean prog timings sim
